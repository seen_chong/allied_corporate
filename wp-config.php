<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'allied-corp_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WP_HOME','http://allied-corp.dev/');
define('WP_SITEURL','http://allied-corp.dev/');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
//define('AUTH_KEY',         ',hT4t:gJNj-_pD6BwtK-gV0v.Y}tSh+uI<NE*,W[Y%#Vs!T$,9NE3_mB}JNi,9P{');
//define('SECURE_AUTH_KEY',  'k>w~Py6KL}WwxbP7_h~LynsAaT32#PDQp/<sf.N~e%!ny6ptpP</=S-&{!|t+hwn');
//define('LOGGED_IN_KEY',    '8QpCMprXxQ8Cf(}QG7| ghLcWzV!_Tz7eJa 15L4,0FjW,b0ODpXRL*fZ<P?g^CG');
//define('NONCE_KEY',        ']_[>VJ$$d8|uUBp#Nc2Z{U51R< X+O[3:v[u*6,]M6X+@E5Rpb4`[h&q^h3e}Lor');
//define('AUTH_SALT',        'rW|+ZS-$(]ZJyo37BQ?Vw#vMsFKbE,?-B$0V67<~~b.uF}WBjZEoX`o,[^NGq{W-');
//define('SECURE_AUTH_SALT', '}>G:.WaU;muy]#;cQi!Y^f!bKe+l}t6k_vUtik0Y+H+V[>NiduCg-O]zLF[sf]?v');
//define('LOGGED_IN_SALT',   '}mWQ#^J?uYhiqZ97soY]l5xNNW@a5fvC|IFpR7//$Pl~Vd,YR`m`74A664iIknC|');
//define('NONCE_SALT',       'kE)v]*H{FX:CgI}1oe}BT?FE4W%?Z;Dfb-#cmzajDCaN|MBRw},[ZK9%wXf=UgH{');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
