<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>



	<footer id="colophon"  role="contentinfo">

			<div class="footer-top">
				<div class="footer-wrapper">
					<div class="width33" id="download-app">
						<a target="_blank" href="https://itunes.apple.com/us/app/alliedpeds/id500183822?mt=8">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/allied-app.png" style="border-radius: 0px;">
						</a>
                        <a target="_blank" href="http://breastfeeding.alliedphysiciansgroup.com/">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/breastfeed_mini_btn.png">
						</a>
<!--
                        <a target="_blank" href="http://nutrition.alliedphysiciansgroup.com/">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/nutrition_mini_button.png">
						</a>
-->
					</div>
					<div class="width33" id="footer-logo">
						<a href="/">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/site-logo.png">
						</a>
					</div>
					<div class="width33" id="footer-social">
						<p><strong>Allied Physicians Group</strong><br>3 Huntington Quadrangle<br>Southeast Tower, Suite #105<br>Melville, NY 11747<br>866.621.2769</p>
					</div>
				</div>
			</div>
			<div class="footer-bottom">
				<div class="footer-wrapper">
				<div class="footer-bottom-left">
					<ul>
						<li><a href="http://alliedphysiciansgroup.com/media">MyLITVFamily</a></li>
						<li>
							<a target="_blank" href="https://vimeo.com/album/3564256">Employee Links</a>
						</li>
						<li>
							<a target="_blank" href="http://www.jobs.net/jobs/alliedphysiciansgroup/en-us/">Careers</a>
						</li>
					</ul>
				</div>
				<div class="footer-bottom-right">
					<p>Allied Physicians Group PLLC 2019</p>
                    <p class="bolt">
                    <a style="color:#fff;text-decoration:underline;"target="_blank" href="https://www.facebook.com/pages/Allied-Physicians-Group-Pediatrics/222129371320227">Trending in Healthcare</a></p>
					<a href="https://www.facebook.com/pages/Allied-Physicians-Group-Pediatrics/222129371320227" target="_blank">
						<img class="social" src="<?php echo get_template_directory_uri(); ?>/assets/img/facebook_wht_64x64.png">
					</a>
                    <a href="https://www.instagram.com/alliedphysiciansgroup/?hl=en" target="_blank">
						<img class="social" src="<?php echo get_template_directory_uri(); ?>/assets/img/instagram.png">
					</a>
					<a href="https://www.youtube.com/channel/UCS8synlMOxqO8nniJ8T99hQ" target="_blank">
						<img class="social" src="<?php echo get_template_directory_uri(); ?>/assets/img/youtube_wht_64x64.png">
					</a>
                    <a href="https://twitter.com/AlliedPhysGroup" target="_blank">
						<img class="social" src="<?php echo get_template_directory_uri(); ?>/assets/img/twitter.png">
					</a>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- .site-wrapper -->


<script>
	$(".img.wp-image-516").click(function(){
  $("p").hide(1000);
});
	</script>
<?php wp_footer(); ?>
</body>
</html>
