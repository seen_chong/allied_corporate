$(document).ready(function(){
	$("#skippr-slider").skippr({ 
	    transition: 'fade', 
	    easing: 'easeOutQuart',  
	    speed: 100,   
		autoPlay: true,
		autoPlayDuration: 6000
	});    
});