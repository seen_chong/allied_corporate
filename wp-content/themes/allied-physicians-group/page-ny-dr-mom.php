<?php
/**
* Template Name: Dr Mom Page
*/

get_header(); ?>

	<div class="blog_container" >
                <div class="blog_entries">
<!--                    ny_dr_mom-->
                    
<?php
   $the_query = new WP_Query( array('posts_per_page'=>30,
                                 'post_type'=>'blog',
                                 'paged' => get_query_var('paged') ? get_query_var('paged') : 1) 
                            ); 
                            ?>
    <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
            <div class="blog_entry">
                <div class="blog_bg" style="background-image:url(<?php the_field('featured_image'); ?>)">
                </div>
                
                <div class="blog_summary">
                <h4><?php the_field('title'); ?></h4>
                    <h6><?php echo get_the_date(); ?> &nbsp;|&nbsp; <?php the_field('author'); ?></h6>
                    <br>
                
                <p><?php the_field('snippet'); ?>...</p>
                <a href="<?php the_permalink(); ?>">
                    <button>Read More</button>
                </a>
                    </div>
            </div>
            
<?php
endwhile;

$big = 999999999; // need an unlikely integer
 echo paginate_links( array(
    'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
    'format' => '?paged=%#%',
    'current' => max( 1, get_query_var('paged') ),
    'total' => $the_query->max_num_pages
) );

wp_reset_postdata();
?>
        </div>
        <div class="blog_sidebar">
                        <h2>Meet The Authors</h2>
            <div class="recent_posts meet_bio">

                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/zilkha.jpeg">
                <p>Dr. Naomi Levine Zilkha was born in the Bronx and raised on Long Island.  She attended Washington University in St. Louis for both college and medical school.  She then returned home, got married and did her residency in pediatrics at Long Island Jewish Schneider Children’s Hospital. After that, Dr. Zilkha was in pediatric private practice on Long Island for nearly 20 years.  In 2015, she began a transition from in-person primary care to pediatric telemedicine.  She has been a pediatric virtualist for the telemedicine company MDLIVE since 2016, and is very excited to now bring her telemedicine expertise to Allied Physicians Group.   Dr. Zilkha and her husband have 2 daughters, Joely and Janna.  In her spare time, she enjoys traveling, organic gardening and ballroom dancing. </p>
                <br>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leveneHS.jpg">
                <p>Dr. Eric Levene grew up on Long Island, New York. He obtained his undergraduate degree in Mathematics from Franklin and Marshall College and received his medical degree from the University of Pittsburgh School of Medicine. He completed his residency at Long Island Jewish Hospital where he then continued for one more year as Chief Resident. He is a practicing partner at Chester Pediatrics in Whiteplains, New York. Dr Levene lives in Armonk, New York with his wife, Marla and two children.</p>
                
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/StephanieRyan.jpg">
                <p>Stephanie Ryan was born and raised on Long Island, New York. She currently resides in Wantagh, NY with her husband - Patrick,  their golden retriever Payton, and their FIVE children-  Jack, Parker, Brandon, Luca & Gabriella. After obtaining her Bachelor's degree in Education at Long Island University at CW Post and teaching for a few years she changed careers and worked in Healthcare Administration. While she thoroughly enjoyed her career in the corporate world, Stephanie decided that her children needed her with them more regularly and she became a stay at home mom. Most recently she decided to return to the classroom part-time at the preschool where her children attend. And now she is sharing stories from life with 5 kids with all of you!</p>
                
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/Macaluso2.jpg">
                <p>Dr. Lauren Macaluso provides specialized medical care for breastfeeding mothers and their children. She is proud to have a medical practice where a mother can get her and her baby’s breastfeeding needs met in one place. She received her Bachelor of Science from Cornell University, her medical degree from Sidney Kimmel Medical College of Thomas Jefferson University in Philadelphia, and completed her pediatric residency at Children’s National Medical Center of George Washington University in Washington, D.C. Dr. Macaluso’s special interest in breastfeeding started after nursing her own two children while working part-time with medical students and residents in the newborn nursery. She identified a need for breastfeeding focused care and started her practice in 2006. </p>
                
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bonnie.jpg">
                <p>Bonnie Fachler is the co-founder of Home Safe Home Childproofing, LLC with her husband Jeffrey.  Together, they have raised two injury-free children who are now both grown and happily married. Bonnie received her degree in Marketing and Business Administration from New York University, is a licensed Physical Therapist Assistant, and an Adjunct Professor at Nassau Community College for the past 11 years.  As an active member of the International Association for Child Safety, she stays current with the latest trends and research in child injury prevention. She is a passionate public speaker reaching parents at nursery schools, daycare centers, parenting centers, and now by writing a monthly blog for Allied Physicians Group. Although education and childproofing have significantly proven to reduce the risk of injury, Bonnie strongly advocates for direct supervision as the primary responsibility of every parent and caretaker.  </p>

                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/rachel.jpg">
                <p>Rachel Levene is the daughter on Dr. Eric Levene and a former #alliedkid. She obtained her undergraduate degree in Microbiology at the University of Rochester and is currently a PhD candidate in molecular microbiology at Tufts University School of Medicine. Her work focuses primarily on influenza virus. She is passionate about all things vaccines and emerging viruses. She is here to help share the newest scientific developments in all things viruses. In her spare time she enjoys baking, reading, and exploring the Boston food and coffee scene. </p>
                <br>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/nydrmom.jpg">
                <p>Most of the kids in my life call me Dr. Bilello.  My three favorites call me mom. I’m Dr. Rachael Green Bilello, pediatrician and mom of three.  I’ve been a general pediatrician on Long Island for 10 years. I have special interests in breastfeeding medicine, nutrition, parenting and adolescent medicine.</p>
                
            </div>
            
            
<!--            <h2>Recent Posts</h2>-->
<!--
            <div class="recent_posts">
                        <//?php
	  			$args = array(
	    		'post_type' => 'ny_dr_mom'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
                        
                <h4><a href="<?php the_permalink(); ?>"><?php the_field('title'); ?></a></h4>
            
            
        <//?php
			}
				}
			else {
			echo 'No Blogs Found';
			}
		?> 
-->
</div>
            
<!--
            <h2>Recent Comments</h2>
            <div class="recent_posts">
                <h4>No Recent Comments.</h4>
            </div>
-->
        </div>
        
        
        

<!--
		<div class="allied-connect-wrapper" id="blog">
            <div class="allied-connect-container">
                <div class="aboutallied inner_content">
                    <?php the_field('content'); ?>

				</div>	
            </div>
        </div>
-->
        



	</div>




<?php
get_footer();
