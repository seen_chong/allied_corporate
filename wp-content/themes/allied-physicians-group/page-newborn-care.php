<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

	<div class="home-container">
        <div class="inner_banner" style="background-image:url(<?php the_field('banner'); ?>)">
                    
        </div>

		<div class="allied-connect-wrapper" id="blog">
            <div class="allied-connect-container">
                <div class="aboutallied inner_content">
                    <h1 class="initial"><?php the_title(); ?></h1>
                    <?php the_field('content'); ?>

				</div>	
            </div>
        </div>


	</div>


<?php
get_footer();
