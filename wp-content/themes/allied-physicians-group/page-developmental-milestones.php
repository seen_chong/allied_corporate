<?php
get_header(); ?>

	<div class="home-container">
                <div class="inner_banner expect_banner" style="background-image:url(<?php the_field('banner'); ?>)">
                    <div class="overlay">
                    
                    
                    <h1 class="page_header">Developmental Milestones</h1>
                        </div>
        </div>
        		<div class="allied-connect-wrapper">
    

		<div class="allied-connect-wrapper">
				<div class="allied-connect-container">
					<div class="age_groups">
                        <div class="age_group_title">
                            <h2>Infancy</h2>
                        </div>
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2888"]'); ?></p>
                        </div>
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2889"]'); ?></p>
                        </div>
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2890"]'); ?></p>
                        </div>
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2891"]'); ?></p>
                        </div>
                        
                    
                        <div class="age_group_title">
                            <h2>Early Childhood</h2>
                        </div>
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2892"]'); ?></p>
                        </div>
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2893"]'); ?></p>
                        </div>
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2894"]'); ?></p>
                        </div>
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2895"]'); ?></p>
                        </div>
                                                <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2896"]'); ?></p>
                        </div>
                                                <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2897"]'); ?></p>
                        </div>
                        
					</div>
			</div>


<?php
get_footer();
