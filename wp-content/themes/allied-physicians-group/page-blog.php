<?php
get_header(); ?>




	<div class="home-container">
                <div class="inner_banner expect_banner" style="background-image:url(<?php the_field('banner'); ?>)">
                    <div class="overlay">
                    
                    
                    <h1 class="page_header">education</h1>
                        </div>
        </div>
        
        <main class="content">
            <div class="entry-content" itemprop="text">
                <div class="display_posts" style="margin-top:0;"><?php echo do_shortcode('[display-posts category="blog" posts_per_page="-1" include_date="true" order="DSC" orderby="date" include_excerpt="true" image_size="large"]'); ?></div>
            </div>
        </main>
        
</div> 

<?php
get_sidebar('in-the-news'); ?>
<?php
get_footer();
