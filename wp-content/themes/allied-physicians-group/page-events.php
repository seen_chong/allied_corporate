<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

<body class="page-template-default page page-id-794 logged-in header-image content-sidebar">

<div class="site-wrapper">
<div class="header-container">
	<header class="home-header" role="banner">
		<div class="site-logo">
			<a href="http://alliedphysiciansgroup.com/">
				<img src="http://alliedphysiciansgroup.com/wp-content/themes/genesis/assets/img/site-logo.png">
			</a>
		</div><!-- .site-branding -->


		<div class="site-navigation">
			<nav class="main-navigation" role="navigation">
				<div class="menu-menu-1-container"><ul id="primary-menu" class="menu"><li id="menu-item-830" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-830"><a href="http://alliedphysiciansgroup.com/specialists/" itemprop="url">Specialists</a></li>
<li id="menu-item-831" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-831"><a href="http://pediatricmed.alliedphysiciansgroup.com/patient-resources/symptom-checker/" itemprop="url">Symptom Checker</a></li>
<li id="menu-item-835" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-794 current_page_item menu-item-835"><a href="http://alliedphysiciansgroup.com/events/" itemprop="url">Events</a></li>
<li id="menu-item-834" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-834"><a href="http://alliedphysiciansgroup.com/in-the-news/" itemprop="url">News</a></li>
<li id="menu-item-1549" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1549"><a href="http://alliedphysiciansgroup.com/blog/" itemprop="url">Blog</a></li>
<li id="menu-item-1515" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1515"><a href="http://alliedphysiciansgroup.com/about/" itemprop="url">About Allied</a></li>
<li id="menu-item-1310" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1310"><a target="_blank" href="http://alliedfoundation.org/" itemprop="url">Allied Foundation</a></li>
</ul></div>
					<ul class="quick-links">
						<li>
							<a id="pay-bills" target="_blank" href="https://www.phreesia.net/25/patient/Payment.aspx/Start?encrypted=nmwSlx4vhmxDdQzPCTvxy6Hjg7oOr5RHcPPwqyYRzRLVAmuEXcBjtIzm0ZgdF5x74yztd7xcyaWuYylFNrnkAUE6fisvmUXdD9PqFJJyKuqsCsfYRx65w7uxkPms1G8udg4JZHVAeRWBYSXUSQPDcAa8A1S0TYTy2vY5vCgKe11caAAIAwkKpbyPWajpYcr08itrYQsrpLYoDSz-0Pdw76rH3xjyrWhDQM-zICA6pQZi1QozjOMWbyVzbuDeXdnA0mCERbg5xkAPcZ8NzsrpjV4LIe0vlXcYG4-bHR54IpJFfUBsusS85phv82sPdXSb0">
								<button>Pay Bills</button>
						</li>
						<li>
							<a target="_blank" href="https://myapny.medconnect.gehealthcare.com/portal/default.aspx">
								<button class="patient-portal">Patient Portal</button>
							</a>
						</li>
						<li>
							<a href="http://corporate.alliedphysiciansgroup.com/">
								<button>Careers</button>
							</a>
						</li>
						<li>
							<a href="/practice">
								<button>Find Practice</button>
							</a>
						</li>
					</ul>
			</nav>
		</div>
	</div>
	</header>

	<header class="mobile_header">
			<button class="hamburger">&#9776;</button>
  <button class="cross">&#735;</button>
	</header>
</div>

<div class="mobile_menu">
	<li>
		<a href="/practice">Find A Practice</a>
	</li>
  <div class="menu-menu-1-container"><ul id="primary-menu" class="menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-830"><a href="http://alliedphysiciansgroup.com/specialists/" itemprop="url">Specialists</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-831"><a href="http://pediatricmed.alliedphysiciansgroup.com/patient-resources/symptom-checker/" itemprop="url">Symptom Checker</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-794 current_page_item menu-item-835"><a href="http://alliedphysiciansgroup.com/events/" itemprop="url">Events</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-834"><a href="http://alliedphysiciansgroup.com/in-the-news/" itemprop="url">News</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1549"><a href="http://alliedphysiciansgroup.com/blog/" itemprop="url">Blog</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1515"><a href="http://alliedphysiciansgroup.com/about/" itemprop="url">About Allied</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1310"><a target="_blank" href="http://alliedfoundation.org/" itemprop="url">Allied Foundation</a></li>
</ul></div>	<li>
		<a href="https://www.phreesia.net/25/patient/Payment.aspx/Start?encrypted=nmwSlx4vhmxDdQzPCTvxy6Hjg7oOr5RHcPPwqyYRzRLVAmuEXcBjtIzm0ZgdF5x74yztd7xcyaWuYylFNrnkAUE6fisvmUXdD9PqFJJyKuqsCsfYRx65w7uxkPms1G8udg4JZHVAeRWBYSXUSQPDcAa8A1S0TYTy2vY5vCgKe11caAAIAwkKpbyPWajpYcr08itrYQsrpLYoDSz-0Pdw76rH3xjyrWhDQM-zICA6pQZi1QozjOMWbyVzbuDeXdnA0mCERbg5xkAPcZ8NzsrpjV4LIe0vlXcYG4-bHR54IpJFfUBsusS85phv82sPdXSb0">Pay Bills</a>
	</li>
	<li>
		<a target="_blank" href="https://myapny.medconnect.gehealthcare.com/portal/default.aspx">Patient Portal
		</a>
	</li>
	<li>
		<a href="http://corporate.alliedphysiciansgroup.com/">Careers</a>
	</li>
	<li>
		<a href="http://alliedphysiciansgroup.com/media">MyLITVFamily</a>
	</li>
</div>

<script>
$( ".cross" ).hide();
$( ".mobile_menu" ).hide();
$( ".hamburger" ).click(function() {
$( ".mobile_menu" ).slideToggle( function() {
$( ".hamburger" ).hide();
$( ".cross" ).show();
});
});

$( ".cross" ).click(function() {
$( ".mobile_menu" ).slideToggle( function() {
$( ".cross" ).hide();
$( ".hamburger" ).show();
});
});
</script><div class="content-sidebar-wrap"><main class="content"><article class="post-794 page type-page status-publish entry" itemscope itemtype="http://schema.org/CreativeWork"><header class="entry-header"><h1 class="entry-title" itemprop="headline">Upcoming Events</h1> 
</header><div class="entry-content" itemprop="text"><div id="margin50" class="row island island-gray">
<h1 style="color: #fff; text-align: center;">Upcoming Events</h1>
</div>
&nbsp;
<div class="upcoming_event_wrapper">
<div class="container-fluid event">
<p style="text-align: center;"><!-- Start Event Date Box --><strong>Babies R Us Lectures </strong></p>
<p style="text-align: center;"><strong>Sign up at <a href="https://reserve.babiesrus.com">reserve.babiesrus.com</a></strong></p>

					<?php
					  $args = array(
					    'post_type' => 'upcoming_events',
					    'posts_per_page' => 99
					    );
					  $products = new WP_Query( $args );
					  if( $products->have_posts() ) {
					    while( $products->have_posts() ) {
					      $products->the_post();
					?>
<!-- Start Event -->


</div>
</div>
<div class="row"></div>
<div class="upcoming_event_wrapper">
<div class="container-fluid event">
<div class="col-md-10 col-sm-10 col-xs-9">
<div class="col-md-10 col-sm-10 col-xs-9"></div>
<div class="row"></div>
<h2></h2>
<div class="upcoming_event_wrapper"></div>
<h2><?php the_field('event_name'); ?></h2>
<p><?php the_field('event_location'); ?></p>
</div>
<div class="row">
<div class="col-md-2 col-sm-2 col-xs-3">
<div class="date-box">
<div class="month"><?php the_field('month'); ?></div>
<div class="date"><?php the_field('date'); ?></div>
<div class="year"><?php the_field('year'); ?></div>
</div>
</div>
<!--End Event Date Box -->

</div>
<!-- End Row 1 -->



<!-- End Event -->
					<?php
				    		}
				  		}
					  else {
					    echo 'No News Found';
					  }
				  	?>	
</div>
</div>

<a class="post-edit-link" href="http://alliedphysiciansgroup.com/wp-admin/post.php?post=794&#038;action=edit">(Edit)</a></div></article></main><aside class="sidebar sidebar-primary widget-area" role="complementary" aria-label="Primary Sidebar" itemscope itemtype="http://schema.org/WPSideBar"><section id="my_calendar_mini_widget-2" class="widget widget_my_calendar_mini_widget"><div class="widget-wrap"><h4 class="widget-title widgettitle">Events Calendar</h4>
<div id="mc_mini_widget-2" class="mc-main mini month mc_mini_widget-2" aria-live='assertive' aria-atomic='true'><div class="my-calendar-header">
				<div class="my-calendar-nav">
					<ul>
						<li class="my-calendar-prev"><a href="http://alliedphysiciansgroup.com/events/?yr=2017&amp;month=5&amp;dy=&amp;cid=mc_mini_widget-2" rel="nofollow" data-rel="mc_mini_widget-2"><span class='maybe-hide'>Previous</span></a></li><li class="my-calendar-next"><a href="http://alliedphysiciansgroup.com/events/?yr=2017&amp;month=7&amp;dy=&amp;cid=mc_mini_widget-2" rel="nofollow" data-rel="mc_mini_widget-2"><span class='maybe-hide'>Next</span></a></li>
					</ul>
				</div><div class="my-calendar-date-switcher">
            <form action="http://alliedphysiciansgroup.com/events/" method="get"><div><input type="hidden" name="cid" value="mc_mini_widget-2" />
            <label class="maybe-hide" for="mc_mini_widget-2-month">Month:</label> <select id="mc_mini_widget-2-month" name="month">
<option value='1'>January</option>
<option value='2'>February</option>
<option value='3'>March</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6' selected="selected">June</option>
<option value='7'>July</option>
<option value='8'>August</option>
<option value='9'>September</option>
<option value='10'>October</option>
<option value='11'>November</option>
<option value='12'>December</option>
</select>

            <label class="maybe-hide" for="mc_mini_widget-2-year">Year:</label> <select id="mc_mini_widget-2-year" name="yr">
<option value="2017" selected="selected">2017</option>
<option value="2018">2018</option>
<option value="2019">2019</option>
<option value="2020">2020</option>
<option value="2021">2021</option>
</select> <input type="submit" class="button" value="Go" /></div>
	</form></div></div>
<table class="my-calendar-table">
<caption class="heading my-calendar-month">June 2017 </caption>
<thead>
<tr class='mc-row'>
<th scope="col" class='day-heading mon'><span aria-hidden='true'><abbr title="Monday">M</abbr></span><span class='screen-reader-text'>Monday</span></th>
<th scope="col" class='day-heading tues'><span aria-hidden='true'><abbr title="Tuesday">T</abbr></span><span class='screen-reader-text'>Tuesday</span></th>
<th scope="col" class='day-heading wed'><span aria-hidden='true'><abbr title="Wednesday">W</abbr></span><span class='screen-reader-text'>Wednesday</span></th>
<th scope="col" class='day-heading thur'><span aria-hidden='true'><abbr title="Thursday">T</abbr></span><span class='screen-reader-text'>Thursday</span></th>
<th scope="col" class='day-heading fri'><span aria-hidden='true'><abbr title="Friday">F</abbr></span><span class='screen-reader-text'>Friday</span></th>

</tr>
</thead>
<tbody><tr class='mc-row'>
												<td class='no-events mon past-day past-date  nextmonth day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>29</span><span class='screen-reader-text'>May 29, 2017</span></span>
												</td>

												<td class='no-events tue past-day past-date  nextmonth day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>30</span><span class='screen-reader-text'>May 30, 2017</span></span>
												</td>

												<td class='no-events wed past-day past-date  nextmonth day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>31</span><span class='screen-reader-text'>May 31, 2017</span></span>
												</td>

												<td class='no-events thu past-day past-date   day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>1</span><span class='screen-reader-text'>June 1, 2017</span></span>
												</td>

												<td class='no-events fri past-day past-date   day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>2</span><span class='screen-reader-text'>June 2, 2017</span></span>
												</td>
<tr class='mc-row'>
												<td class='no-events mon past-day past-date   day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>5</span><span class='screen-reader-text'>June 5, 2017</span></span>
												</td>

												<td class='no-events tue past-day past-date   day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>6</span><span class='screen-reader-text'>June 6, 2017</span></span>
												</td>

												<td class='no-events wed past-day past-date   day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>7</span><span class='screen-reader-text'>June 7, 2017</span></span>
												</td>

												<td class='no-events thu past-day past-date   day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>8</span><span class='screen-reader-text'>June 8, 2017</span></span>
												</td>

												<td class='no-events fri past-day past-date   day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>9</span><span class='screen-reader-text'>June 9, 2017</span></span>
												</td>
<tr class='mc-row'>
												<td class='no-events mon past-day past-date   day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>12</span><span class='screen-reader-text'>June 12, 2017</span></span>
												</td>

												<td class='no-events tue past-day past-date   day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>13</span><span class='screen-reader-text'>June 13, 2017</span></span>
												</td>

												<td class='no-events wed past-day past-date   day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>14</span><span class='screen-reader-text'>June 14, 2017</span></span>
												</td>

												<td class='no-events thu past-day past-date   day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>15</span><span class='screen-reader-text'>June 15, 2017</span></span>
												</td>

												<td class='no-events fri past-day past-date   day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>16</span><span class='screen-reader-text'>June 16, 2017</span></span>
												</td>
<tr class='mc-row'>
												<td class='no-events mon past-day past-date   day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>19</span><span class='screen-reader-text'>June 19, 2017</span></span>
												</td>

												<td class='no-events tue current-day   day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>20</span><span class='screen-reader-text'>June 20, 2017</span></span>
												</td>

												<td class='no-events wed future-day   day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>21</span><span class='screen-reader-text'>June 21, 2017</span></span>
												</td>

												<td class='no-events thu future-day   day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>22</span><span class='screen-reader-text'>June 22, 2017</span></span>
												</td>

												<td class='no-events fri future-day   day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>23</span><span class='screen-reader-text'>June 23, 2017</span></span>
												</td>
<tr class='mc-row'>
												<td class='no-events mon future-day   day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>26</span><span class='screen-reader-text'>June 26, 2017</span></span>
												</td>

												<td class='no-events tue future-day   day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>27</span><span class='screen-reader-text'>June 27, 2017</span></span>
												</td>

												<td class='no-events wed future-day   day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>28</span><span class='screen-reader-text'>June 28, 2017</span></span>
												</td>

												<td class='no-events thu future-day   day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>29</span><span class='screen-reader-text'>June 29, 2017</span></span>
												</td>

												<td class='no-events fri future-day   day-with-date'>
													<span class='mc-date no-events'><span aria-hidden='true'>30</span><span class='screen-reader-text'>June 30, 2017</span></span>
												</td>

</tbody>
</table></div></div></section>
<section id="sfsi-widget-2" class="widget sfsi"><div class="widget-wrap"><h4 class="widget-title widgettitle">Please follow &#038; like us :)</h4>
            <div class="sfsi_widget" data-position="widget">   
				<div id='sfsi_wDiv'></div>
                    <div class="norm_row sfsi_wDiv"  style="width:225px;text-align:left;position:absolute;"><div style='width:40px; height:40px;margin-left:5px;margin-bottom:5px;' class='sfsi_wicons shuffeldiv '><div class='inerCnt'><a class=' sficn' effect='' target='_blank'  href='http://www.specificfeeds.com/widgets/emailSubscribeEncFeed/YzB5dEdSeDZ0QzEvd2R1TFBSZ1J4YTdhdVk0YmxqN3FrZnBmM2NnUVVnQmpaS1F1a0Z3WUFyTE5HS0w1OVZvV0V6c1czWS9uMGZ4NUdNMnUwSk4vTlhaYjFsOUNDa1VxZmRDV2E2dUwvc2F1SncvaFNvcXp3NmtlQTlxSjd4Wm98Mi82aDhUdS9KMDRPdUE0Nml1bUNUSUpINGZRRU9yWTU0eEVHRm5JSHRxaz0=/OA==/' id='sfsiid_email' alt='Follow by Email' style='opacity:1' ><img alt='Follow by Email' title='Follow by Email' src='http://alliedphysiciansgroup.com/wp-content/plugins/ultimate-social-media-icons/images/icons_theme/default/default_email.png' width='40' height='40' style='' class='sfcm sfsi_wicon' effect=''   /></a></div></div><div style='width:40px; height:40px;margin-left:5px;margin-bottom:5px;' class='sfsi_wicons shuffeldiv '><div class='inerCnt'><a class=' sficn' effect='' target='_blank'  href='https://www.facebook.com/pages/Allied-Physicians-Group-Pediatrics/222129371320227' id='sfsiid_facebook' alt='Facebook' style='opacity:1' ><img alt='Facebook' title='Facebook' src='http://alliedphysiciansgroup.com/wp-content/plugins/ultimate-social-media-icons/images/icons_theme/default/default_facebook.png' width='40' height='40' style='' class='sfcm sfsi_wicon' effect=''   /></a><div class="sfsi_tool_tip_2 fb_tool_bdr sfsiTlleft" style="width:62px ;opacity:0;z-index:-1;margin-left:-47.5px;" id="sfsiid_facebook"><span class="bot_arow bot_fb_arow"></span><div class="sfsi_inside"><div  class='icon2'><div class="fb-like" data-href="http://alliedphysiciansgroup.com/events/" data-layout="button" data-action="like" data-show-faces="false" data-share="true"></div></div><div  class='icon3'><div class="fb-share-button" data-href="http://alliedphysiciansgroup.com/events/" data-layout="button"></div></div></div></div></div></div><div style='width:40px; height:40px;margin-left:5px;margin-bottom:5px;' class='sfsi_wicons shuffeldiv '><div class='inerCnt'><a class=' sficn' effect='' target='_blank'  href='https://www.youtube.com/channel/UCS8synlMOxqO8nniJ8T99hQ' id='sfsiid_youtube' alt='YOUTUBE' style='opacity:1' ><img alt='YOUTUBE' title='YOUTUBE' src='http://alliedphysiciansgroup.com/wp-content/plugins/ultimate-social-media-icons/images/icons_theme/default/default_youtube.png' width='40' height='40' style='' class='sfcm sfsi_wicon' effect=''   /></a><div class="sfsi_tool_tip_2 utube_tool_bdr sfsiTlleft" style="width:96px ;opacity:0;z-index:-1;margin-left:-64.5px;" id="sfsiid_youtube"><span class="bot_arow bot_utube_arow"></span><div class="sfsi_inside"><div  class='icon2'><div class="g-ytsubscribe" data-channel="Allied Physicians Group" data-layout="default" data-count="hidden"></div></div></div></div></div></div></div ><div id="sfsi_holder" class="sfsi_holders" style="position: relative; float: left;width:100%;z-index:-1;"></div ><script>jQuery(".sfsi_widget").each(function( index ) {
					if(jQuery(this).attr("data-position") == "widget")
					{
						var wdgt_hght = jQuery(this).children(".norm_row.sfsi_wDiv").height();
						var title_hght = jQuery(this).parent(".widget.sfsi").children(".widget-title").height();
						var totl_hght = parseInt( title_hght ) + parseInt( wdgt_hght );
						jQuery(this).parent(".widget.sfsi").css("min-height", totl_hght+"px");
					}
				});</script>	      		<div style="clear: both;"></div>
            </div>
            </div></section>
<section id="text-3" class="widget widget_text"><div class="widget-wrap">			<div class="textwidget"><!-- Start Primary Callouts Wrap -->
<div class="container-fluid-mobile">
<div class="row"><div class="border-top-halo-gray" style="margin-bottom:0;"></div></div>
<div class="row sidebar-primary-callout-wrap">
<div class="col-md-12"><h4>Are you a patient?</h4></div>
<div class="col-md-12"><div class="patient-callout">Visit a specialty site in our network:</div></div>
<div class="col-md-12"><a href="http://pediatricmed.alliedphysiciansgroup.com/"><div class="primary-callout pc-apg-blue link-fade"><span>Pediatric <br> Medicine</span></div><div class="btm-left-notch"></div></a></div>

<div class="col-md-12"><a href="/specialists/"><div class="primary-callout pc-apg-pink link-fade single-line"><span>Specialists</span></div><div class="btm-left-notch"></div></a></div>
</div>
</div>
<!-- End Primary Callouts Wrap-->
</div>
		</div></section>
<section id="text-19" class="widget widget_text"><div class="widget-wrap">			<div class="textwidget"><div class="container-fluid-mobile">
<div class="row island island-gray">
<div class="island-gray-header"><span>Are you a healthcare professional?</span></div>
<p>Join the extensive network of physicians at Allied Physicians Group today!</p>
<a href="http://corporate.alliedphysiciansgroup.com/grow-with-us/"><div class="btn link-fade">Grow With Us</div></a>
</div>
</div></div>
		</div></section>
<section id="text-21" class="widget widget_text"><div class="widget-wrap">			<div class="textwidget"><div class="container-fluid-mobile">
<div class="row island island-gray">
<div class="island-gray-header"><span>Pay your bill online!</span></div>
<a href="https://www.phreesia.net/25/patient/Payment.aspx/Start?encrypted=nmwSlx4vhmxDdQzPCTvxy6Hjg7oOr5RHcPPwqyYRzRLVAmuEXcBjtIzm0ZgdF5x74yztd7xcyaWuYylFNrnkAUE6fisvmUXdD9PqFJJyKuqsCsfYRx65w7uxkPms1G8udg4JZHVAeRWBYSXUSQPDcAa8A1S0TYTy2vY5vCgKe11caAAIAwkKpbyPWajpYcr08itrYQsrpLYoDSz-0Pdw76rH3xjyrWhDQM-zICA6pQZi1QozjOMWbyVzbuDeXdnA0mCERbg5xkAPcZ8NzsrpjV4LIe0vlXcYG4-bHR54IpJFfUBsusS85phv82sPdXSb0" target="_blank"><div class="btn link-fade">Make a Payment</div></a>
</div>
</div></div>
		</div></section>
<section id="text-12" class="widget widget_text"><div class="widget-wrap">			<div class="textwidget"><div class="container-fluid-mobile">
<div class="row island island-gray">
<div class="island-gray-header"><span>Our Events</span></div>
<p>Stay in the know regarding our upcoming events!</p>
<a href="/events/"><div class="btn link-fade">See All Events</div></a>
</div>
</div></div>
		</div></section>
<section id="text-18" class="widget widget_text"><div class="widget-wrap">			<div class="textwidget"><div class="container-fluid-mobile">
<div class="row"><div class="border-top-halo-facebook-blue"></div>
<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FAllied-Physicians-Group-Pediatrics%2F222129371320227&amp;width&amp;height=650&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:600px;width:100%;margin-bottom: 30px;" allowTransparency="true"></iframe></div>
</div></div>
		</div></section>
</aside></div>
<?php
get_footer();