<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

<!-- Segment Pixel - Allied Physicians Group/Adjuvant - DO NOT MODIFY -->
<script src="https://secure.adnxs.com/seg?add=19363560&t=1" type="text/javascript"></script>
<!-- End of Segment Pixel -->

<!--                    <h1><?php the_title(); ?></h1>-->
<div class="site-wrapper">
        <div class="inner_banner" id="default" style="background-image:url(<?php the_field('banner'); ?>)">
            <h1><?php the_title(); ?></h1>
        </div>
    
	<div class="home-container">

		<div class="allied-connect-wrapper">
            <div class="allied-connect-container">
                <div class="aboutallied inner_content" id="what_to_expect">
                    <p><?php the_field('intro'); ?></p>
                    <div class="content_tabs">
                        <ul>
                            
                            <li><a href="#allergy"><p>Allergy & Asthma</p></a></li>
                            <li><a href="#behavioral"><p>Behavioral Medicine</p></a></li>
                            <li><a href="#breastfeeding"><p>Breastfeeding</p></a></li>
                            <li><a href="#nutritionists"><p>Nutritionists</p></a></li>
                            <li><a href="#sleep"><p>Pulmonology/Sleep Medicine</p></a></li>
                        </ul>
                    </div>
                    <div>
                        <?php the_field('content'); ?>
                    </div>


				</div>	
            </div>
        </div>


	</div>
    
  <script>
    $(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top - 80
    }, 500);
});
    </script>  

<!--    <//?php get_sidebar();?>-->
<?php
get_footer();