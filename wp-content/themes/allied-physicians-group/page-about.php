<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

	<div class="home-container">


		<div class="allied-connect-wrapper">
            <div class="allied-connect-container">
                <div class="aboutallied">
                    <h1>About Allied</h1>
	               <?php the_field('video'); ?>
                    <?php the_field('content'); ?>

				</div>	
            </div>
        </div>


	</div>


<?php
get_footer();
