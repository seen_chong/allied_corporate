<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

	<div class="home-container">
		<div class="swiper-container">
		    <!-- Additional required wrapper -->
		    <div class="swiper-wrapper">
		        <!-- Slides -->
                
                    <?php
					  $args = array(
					    'post_type' => 'home_sliders',
					    'posts_per_page' => 5
					    );
					  $products = new WP_Query( $args );
					  if( $products->have_posts() ) {
					    while( $products->have_posts() ) {
					      $products->the_post();
					?>
		        <div class="swiper-slide" style="background-image:url(<?php the_field('slider_image'); ?>)">
		        	<div class="home-hero-call">
						<!-- <h4>Partner Physicians</h4> -->
						<h3><?php the_field('title'); ?></h3>
						<h4><?php the_field('subtitle'); ?></h4>
                        <?php if (get_field('button_text') != ''): ?>
						<a href="<?php the_field('button_link'); ?>">
							<button><?php the_field('button_text'); ?></button>
						</a>
                        <?php endif; ?>
					</div>
		        </div>
                
                					<?php
				    		}
				  		}
					  else {
					    echo 'No Media Found';
					  }
				  	?>	

		    </div>
		    <!-- If we need pagination -->
		    <div class="swiper-pagination"></div>
            
<!--
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
-->
		    
		    <!-- If we need scrollbar -->
		    <div class="swiper-scrollbar"></div>
		</div>

		<div class="allied-connect-wrapper">
				<div class="allied-connect-container">
					<div class="mid-section-links">
                        <a href="http://breastfeeding.alliedphysiciansgroup.com/" target="_blank">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/breastfeed_button.png" >
						</a>
						<a href="http://pediatricmed.alliedphysiciansgroup.com/patient-resources/symptom-checker/">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/checker_new.png">
						</a>
<!--
                        <a href="http://nutrition.alliedphysiciansgroup.com/" target="_blank">
							<img src="<//?php echo get_template_directory_uri(); ?>/assets/img/nutrition_button.png" >
						</a>
-->
					</div>

					<div class="center-inline">
					<div class="width33" id="in-the-news">
						<h4>In The News</h4>

						<div class="inner-connect-wrapper">
							<ul>
					<?php
					  $args = array(
					    'post_type' => 'in_the_news',
					    'posts_per_page' => 5 
					    );
					  $products = new WP_Query( $args );
					  if( $products->have_posts() ) {
					    while( $products->have_posts() ) {
					      $products->the_post();
					?>
								<li>
									<h6><?php the_field('published'); ?></h6>
									<p><span><?php the_field('headline'); ?><br></span> <a target="_blank" href="<?php the_field('link'); ?>">Read More</a></p>
								</li>
					<?php
				    		}
				  		}
					  else {
					    echo 'No News Found';
					  }
				  	?>	

							</ul>
						</div>

						<a href="/news/">
							<button>More News</button>
						</a>
					</div>

<!--
					<div class="width33" id="upcoming-events">
						<h4>Upcoming Events</h4>

					<div class="inner-connect-wrapper">
						<ul>
					<?php
					  $args = array(
					    'post_type' => 'upcoming_events',
					    'posts_per_page' => 3
					    );
					  $products = new WP_Query( $args );
					  if( $products->have_posts() ) {
					    while( $products->have_posts() ) {
					      $products->the_post();
					?>
e
								<li>
									<h6><?php the_field('month'); ?> <?php the_field('date'); ?></h6>
									 <h2><?php the_field('event_name'); ?></h2> 
									<p><span><?php the_field('event_name'); ?><br></span><p><?php the_field('event_location'); ?></p></p>
								</li>



					<?php
				    		}
				  		}
					  else {
					    echo 'No Events Found';
					  }
				  	?>	
				  	</ul>
				  	</div>											
						<a href="/events/">
							<button>More Events</button>
						</a>
					</div>
-->
					<div class="width33" id="contact-us">
						<h4>Events</h4>
						<div class="inner-connect-wrapper">
                            <?php echo do_shortcode('[events_list]'); ?>
<!--
                            
                            
							<ul>
                                    <//?php
  $args = array(
    'post_type' => 'spotlight',
    'posts_per_page' => 99
    );
  $products = new WP_Query( $args );
  if( $products->have_posts() ) {
    while( $products->have_posts() ) {
      $products->the_post();
?>
								<li>
                                    <p>
                                        <span><//?php the_field('title'); ?></span>
                                    </p>
                                    <p><//?php the_field('summary'); ?><br><a href="/education">See More</a></p>
								</li>
<//?php
        }
    }
  else {
    echo 'No Entries Found';
  }
?>	
					
							</ul>
						</div>
                        						<a href="/education/">
							<button>More Education</button>
						</a>
-->
					</div>
            
            				</div>		

					</div>	
			</div>

		<div class="mid-section-block" id="block-1" style="display:none;">
			<div class="mid-section-wrapper">

				<div class="mid-section-header">

					
					<div class="header-left">
						<img class="white-line" src="<?php echo get_template_directory_uri(); ?>/assets/img/white-line.png">
						<h2>Telehealth</h2>
					</div>

					<div class="header-right">
						<a href="/telehealth">
							<button>Learn More</button>
						</a>
					</div>
				</div>
				
				<div class="mid-section-content">
					<div class="content-container">
					
                        <a href="/telehealth">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/tytoalliedhomepage.jpg">
                            </a>
<!--
						<div class="mid-section-text">
						
							<h6>A Comfortable Environment</h6>
							<h4>The Allied Family of pediatric doctors</h4>
							<p>Experienced, dedicated and caring physicians for you and your family.</p>
							<a href="/telehealth">
								<img class="go-button" src="<?php echo get_template_directory_uri(); ?>/assets/img/button-go.png">
							</a>
						</div>
--></div>
						
				</div>

			</div>
		</div>

<!--
		<div class="mid-section-block" id="block-2">
			<div class="mid-section-wrapper">

				<div class="mid-section-header">
					<div class="header-left">
						<img class="white-line" src="<?php echo get_template_directory_uri(); ?>/assets/img/white-line.png">
						<h2>Medical Specialists</h2>
					</div>

					<div class="header-right">
						<a href="/specialists/">
							<button>Learn More</button>
						</a>
					</div>
				</div>
				
				<div class="mid-section-content">

					<div class="mid-section-text">
						
						<h6>Unique Skillsets</h6>
						<h4>A growing network of specialized physicians</h4>
						<p>Determined to provide you and your family with the most comprehensive, complete, and personalized medical care possible.</p>
						<a href="/specialists/">
							<img class="go-button" src="<?php echo get_template_directory_uri(); ?>/assets/img/button-go-yell.png">
						</a>
					</div>
					<div class="content-container" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/doctors_group.jpg)">
					</div>
				</div>
			</div>

		</div>
-->

		<div class="allied-connect">
			<div class="allied-connect-wrapper-offset">
				<div class="header">
					<h3>Testimonials</h3>
                    <div class="feedback_slider">
                                            <?php
					  $args = array(
					    'post_type' => 'testimonials',
					    'posts_per_page' => 99
					    );
					  $products = new WP_Query( $args );
					  if( $products->have_posts() ) {
					    while( $products->have_posts() ) {
					      $products->the_post();
					?>
                        <div class="feedback">
                            <p>"<?php the_field('review'); ?>"</p>
                            <br><br>
                            <p><strong>-<?php the_field('reviewer'); ?></strong></p>
                        </div>
                    <?php
				    		}
				  		}
					  else {
					    echo 'No Testimonials Found';
					  }
				  	?>	
                    </div>
<!--					<//?php echo do_shortcode('[events_list]'); ?>-->
				</div>
				<a href="/practice">
				<div class="allied-map" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/map-practices-new-alt.png)"></div>
				</a>
			</div>
			
		</div>

	</div>
<style>
@media screen and (max-width: 1040px) {
.allied-connect-container .width33 {
    width: 100%;
}
}
</style>
<script>
       $('.feedback_slider').slick({
  dots: false,
           arrows:true,
  infinite: true,
  speed: 700,
  autoplay: true,
  autoplaySpeed: 3500,
  slidesToShow: 1,
  adaptiveHeight: true
}); 
</script>
<script>
var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    slidesPerView: 1,
    paginationClickable: true,
    loop: true,
    autoplay:5000,
    nextButton: ".swiper-button-next",
prevButton: ".swiper-button-prev",
    breakpoints: {
	    870: {
	        slidesPerView: 1
	    }
	}
});
</script>
        

<?php
get_footer();
