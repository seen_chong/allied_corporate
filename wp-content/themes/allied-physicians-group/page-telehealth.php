<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

	<div class="home-container telehealth">
        <div class="telehealth_banner" style="background-image:url(<?php the_field('banner'); ?>)">
            <div class="flex">
            <h1 class="page_header telehealth_header"><?php the_title(); ?></h1>
            </div>
        </div>

		<div class="allied-connect-wrapper allied_grey">
            <div class="allied-connect-container">

                <!-- <div class="tele_nav">
                    <a href="#symptoms"><h4>Covid Symptoms</h4></a>
                    <a href="#telehealth_allied"><h4>Telehealth at Allied Physicians Group</h4></a>
                    <a href="#gettingstarted"><h4>Getting Started</h4></a>
                    <a href="#codes"><h4>Find Your Practice Code</h4></a>
                </div> -->
                <div class="tele_content" id="symptoms">
                    <!-- <h2><span>Covid</span> Symptoms</h2> -->
                    <?php the_field('block_one'); ?>
                    <h3><span><?php echo do_shortcode('[popup_anything id="3525"]'); ?></span></h3>
				</div>	
            </div>
        </div>

        <div class="tele_content">
        <div class=" tele_grid" id="telehealth_allied">
            <div class="tele_text">
                <div class="tele_wrap">
                    <h2>Telehealth at Allied Physicians <span>Group</span></h2>
                    <?php the_field('grid_text_one'); ?>
                </div>
            </div>
            <div class="tele_img" style="background-image:url(<?php the_field('grid_image_one'); ?>)">
            </div>
        </div>

            <div class=" tele_grid" id="telehealth_nonallied">
                <div class="tele_img" style="background-image:url(<?php the_field('grid_image_two'); ?>)"></div>
                <div class="tele_text">
                    <div class="tele_wrap">
                        <h2>New Telehealth Pediatric Patients, Non-Allied <span>Families</span></h2>
                        <?php the_field('grid_text_two'); ?>
                    </div>
                </div>
            </div>
            <div class="tele_column" id="gettingstarted" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/gettingstarted.png)">
            <div class="tele_text tele_text_right">
                <div class="tele_wrap">
                    <h2><span>Getting</span> Started</h2>
                    <?php the_field('getting_started'); ?>
                </div>
            </div>
        </div>
        <div class="allied-connect-wrapper">
            <div class="allied-connect-container">
                <div class="tele_content" id="codes">
                    <h2><span>Practice</span> Codes</h2>
                    <?php the_field('practice_codes'); ?>

                    <div class="allied_grid">
                    <div class="county_grid">
                    <?php the_field('county_one'); ?>
                    </div>
                    <div class="county_grid">
                    <?php the_field('county_two'); ?>
                    </div>
                    <div class="county_grid">
                    <?php the_field('county_three'); ?>
                    </div>

                    <div class="county_grid">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/nystate.png">
                    </div>
                    </div>
				</div>	
            </div>
        </div>
        </div>




	</div>

<style>
.allied_grid {
    flex-wrap:wrap;
    display:flex;
}
.allied_grid ul {
    flex:0 50%;
    margin-bottom:60px;
}
p.p_fifty {
    width:50%;
}
.tele_column {
    width:100%;
    height:700px;
    background-size:cover;
    background-repeat:no-repeat;
}
.telehealth_banner {
    width:100%;
    height:80vh;
    background-size:cover;
    background-blend-mode: multiply;
    background-color: rgba(0, 0, 0, 0.3);
}
.telehealth_banner .flex {
    width:90%;
    height:100%;
    margin:0 auto;
    display:flex;
    align-items:baseline;
    justify-content:flex-end;
    flex-direction:column;
}
.home-container.telehealth {
    margin-top:81px;
}
.telehealth_banner h1{
    color:#fff;
    font-size:48px;
    text-transform:uppercase;
}
.telehealth_banner h3 {
    font-size:24px;
    color:#fff;
    padding-bottom:10px;
    line-height:3em;
}
.telehealth_banner h3 a {
    color:#fff;
}
.telehealth_banner h3 span {
    border-bottom: 1px solid #fff;
}
.tele_nav {
    display:flex;
    justify-content:space-between;
    padding-bottom:100px;
}
.tele_nav a {
    color:#ea5b31;
}
.tele_nav a h4 {
    color:#ea5b31;
    border-bottom:0;
    width:80%;
    margin:0 auto;
    text-align:center;
}

.tele_content h2 {
    color:#ea5b31;
    font-size:32px;
    padding-bottom:10px;
}
.tele_content h2 span {
    border-bottom:1px solid #ea5b31;
}
.telehealth p,
.telehealth li {
    font-size:16px;
}
ul.checklist  {
    display:flex;
    flex-wrap:wrap;
}
ul.checklist li  {
    flex:0 50%;
    line-height:2em;
}
ul.checklist li p {
    display:inline-block;
    margin-left:10px;
}
ul.checklist li:before {
    content: '✓';
    font-size:20px;
    display:inline-block;
    margin-right:10px;
}
.tele_grid {
    display: flex;
    flex-wrap:wrap;
}
.tele_text {
    padding-top:30px;
    flex:0 50%;
    height:100%;
}
.tele_text.tele_text_right .tele_wrap{
    margin-left:auto;
    margin-right:0;
    width:50%;
    justify-content:center;

}
.tele_text.tele_text_right .tele_wrap p {
    width:95%;
    margin-bottom:7px;
}
.tele_text.tele_text_right .tele_wrap img {
    width:150px;
    display:inline-block;
}
.tele_text.tele_text_right h2 {
    color:#ea5b31;
}
.county_grid h5 {
    color:#ea5b31;
    font-weight:600;
}
.county li p {
    margin-bottom:5px;
}
.tele_text.tele_text_right p {
    color:#fff;
}
.tele_text p {
    line-height:1.25em;
    margin-bottom:10px;
}
.tele_text .tele_wrap {
    width:80%;
    margin:0 auto;
}
.tele_img {
    height:550px;
    background-size:cover;
    background-repeat:no-repeat;
    flex:0 50%;
}
.county_grid {
    flex:0 50%;
}
.county_grid ul {
    width:90%;
}
.county_grid ul li a {
    color:inherit;
}

@media only screen and (max-width: 767px ){
    ul.checklist li {
        flex:100%;
    }
    .tele_text.tele_text_right .tele_wrap p {
        width:100%;
    }
    .county_grid {
        flex:100%;
    }
    .tele_text,
    .tele_img {
        flex:auto;
    }
    .tele_img{
        width:100%;
        margin:0 auto;
    }
    div#telehealth_allied {
        flex-direction:column-reverse;
    }
    .tele_text.tele_text_right .tele_wrap {
        width:95%;
        margin:0 auto
    }
}
</style>

<?php
get_footer();
