<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

	<div class="home-container">
            <div class="inner_banner" id="default" style="background-image:url(<?php the_field('banner'); ?>)">
                            <h1 style="text-align:center;color:#fff;"><?php the_field('banner_title'); ?></h1>
        </div>

		<div class="allied-connect-wrapper" id="blog">
            <div class="allied-connect-container">
                <div class="aboutallied inner_content">
                    						<div class="inner-connect-wrapper" style="height:auto;">
							<ul>
					<?php
					  $args = array(
					    'post_type' => 'in_the_news',
					    'posts_per_page' => 99
					    );
					  $products = new WP_Query( $args );
					  if( $products->have_posts() ) {
					    while( $products->have_posts() ) {
					      $products->the_post();
					?>
								<li>
									<h6><?php the_field('published'); ?></h6>
									<p><span><?php the_field('headline'); ?><br></span> <a target="_blank" style="color:#ea5b31;" href="<?php the_field('link'); ?>">Read More</a></p>
								</li>
					<?php
				    		}
				  		}
					  else {
					    echo 'No News Found';
					  }
				  	?>	

							</ul>
						</div>

				</div>	
            </div>
        </div>


	</div>


<?php
get_footer();
