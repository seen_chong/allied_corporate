<?php
get_header(); ?>

	<div class="home-container">
                <div class="inner_banner expect_banner" style="background-image:url(<?php the_field('banner'); ?>)">
                    <div class="overlay">
                    
                    
                    <h1 class="page_header">what to expect at your next visit</h1>
                        </div>
        </div>
        		<div class="allied-connect-wrapper">
    

		<div class="allied-connect-wrapper">
				<div class="allied-connect-container">
					<div class="age_groups">
                        <div class="pdf_dl">
                        <h2><a href="https://alliedphysiciansgroup.com/wp-content/uploads/2019/07/Newborn-Handout.pdf" target="_blank">Click here to download our Caring for our newborn information sheet</a></h2>
                            </div>
                        <div class="age_group_title">
                            
                            <h2>Infancy</h2>
                            <p><a href="<?php the_field('infancy_pdf'); ?>" target="_blank">Click here to print the full Infancy chart</a></p>
                        </div>
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2570"]'); ?></p>
                        </div>
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2605"]'); ?></p>
                        </div>
                        
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2571"]'); ?></p>
                        </div>
                        
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2572"]'); ?></p>
                        </div>
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2573"]'); ?></p>
                        </div>
                        <div class="age_group_title">
                            <h2>Early Childhood</h2>
                            <p><a href="<?php the_field('early_childhood_pdf'); ?>" target="_blank">Click here to print the full Early Childhood chart</a></p>
                        </div>
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2574"]'); ?></p>
                        </div>
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2575"]'); ?></p>
                        </div>
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2576"]'); ?></p>
                        </div>
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2577"]'); ?></p>
                        </div>
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2578"]'); ?></p>
                        </div>
                        <div class="age_group_title">
                            <h2>Middle Childhood</h2>
                            <p><a href="<?php the_field('middle_childhood_pdf'); ?>" target="_blank">Click here to print the full Middle Childhood chart</a></p>
                        </div>
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2616"]'); ?></p>
                        </div>
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2617"]'); ?></p>
                        </div>
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2579"]'); ?></p>
                        </div>
                        
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2580"]'); ?></p>
                        </div>
                        <div class="age_group_title">
                            <h2>Adolescence</h2>
                            <p><a href="<?php the_field('adolescence_childhood_pdf'); ?>" target="_blank">Click here to print the full Adolescence chart</a></p>
                        </div>
                        <div class="age_button">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leaf.png">
                            <p><?php echo do_shortcode('[popup_anything id="2581"]'); ?></p>
                        </div>
                        
					</div>
			</div>


<?php
get_footer();
