<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

<body class="page-template-default page page-id-794 logged-in header-image content-sidebar">
    
<div class="content-sidebar-wrap">
<main class="content"><article class="post-1421 page type-page status-publish entry" itemscope itemtype="http://schema.org/CreativeWork">
    <div class="entry-content" itemprop="text"><div class="row island island-gray" id="margin50" style="text-align:center;">
        <header class="entry-header"><h1 class="entry-title" itemprop="headline">Education</h1></header>
    </div>
<br>
<br>
<br>


    
    <?php
  $args = array(
    'post_type' => 'spotlight',
    'posts_per_page' => 99
    );
  $products = new WP_Query( $args );
  if( $products->have_posts() ) {
    while( $products->have_posts() ) {
      $products->the_post();
?>
        <!-- Start Event -->
<div class="past_event_wrapper"> 
<div class="container-fluid event">

<!-- Start Row 1 -->
<div class="row">

<!-- Start Event Title -->
    <a href="<?php the_field('link'); ?>" target="_blank">
        <h2><?php the_field('title'); ?></h2>
    </a>
<!-- End Event Title -->

</div>
    
<!-- End Row 1 -->

</div>
    <a href="<?php the_field('link'); ?>" target="_blank">
<img src="<?php the_field('image'); ?>">
    </a>
</div>
    
<!-- End Event -->
<?php
        }
    }
  else {
    echo 'No Entries Found';
  }
?>	

<style>
.container-fluid.event:first-child {height:150px;}
</style><a class="post-edit-link" href="http://alliedphysiciansgroup.com/wp-admin/post.php?post=1421&#038;action=edit">(Edit)</a></div></article></main>
    </div>
</body>
<?php
get_footer();