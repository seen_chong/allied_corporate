<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}
if (defined('SABRES_PLUGIN_DIR')) {
   require_once SABRES_PLUGIN_DIR.'/_inc/class.singleton.php';
   require_once SABRES_PLUGIN_DIR.'/_inc/settings.php';
   require_once SABRES_PLUGIN_DIR.'/_inc/class.singleton.php';
   require_once SABRES_PLUGIN_DIR.'/_inc/class.logger.php';
 }



/*
 * Copyright 2016 Sabres Security Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


if ( !class_exists( 'SBS_Server' ) ) {

    /**
     * The Sabres Server Class
     *
     * @author Ariel Carmely - Sabres Security Team
     * @package Sabres_Security_Plugin
     * @since 1.0.0
     */
    final class SBS_Server extends SBS_Singleton
    {
        const SERVER_API_URL = 'https://sa-gateway.sabressecurity.com';
        const SERVER_API_URL_PLAIN = 'http://sa-gateway.sabressecurity.com';
        protected static $instance;

        function __construct()
        {
        }

        public function call( $code = '', $action = null, $data = null )
        {
            $logger = SBS_Logger::getInstance();
            $settings=SbrSettings::instance();

            if ( !empty( $data ) ) {
                $params = $data;
            } else {
                $params = array();
            }

            if ( !empty( $action ) ) {
                $params['action'] = $action;
            }

            $request_info = array(
                'method' => 'POST',
                'timeout' => 10,
                'redirection' => 5,
                'httpversion' => '1.0',
                'sslverify' => false,
                'blocking' => true,
                'headers' => array(),
                'body' => $params,
                'cookies' => array()
            );

            $url = $this->get_server_api_url() . "/$code";

            $server_offline = false;

            $response_info = wp_remote_post( $url, $request_info );


            if ( is_wp_error( $response_info ) ) {
                $log_type = 'error';
                $log_message = $response_info->get_error_message();

                $server_offline = true;
            } else {
                if ( !empty( $response_info['response']['code'] ) ) {
                    $response_code = $response_info['response']['code'];
                }

                if ( !empty( $response_info['response']['message'] ) ) {
                    $response_message = $response_info['response']['message'];
                }              
                if ( $response_code !== 200 &&  $code!='heartbeat') {
                    $log_type = 'error';
                    $log_message = $response_message;
                    $server_offline = true;
                } else {
                    if ( !empty( $response_info['body'] ) ) {
                        $response_body = @json_decode( $response_info['body'] );

                        if ( isset( $response_body->error ) ) {
                            $log_type = 'error';
                            $log_message = $response_body->error;
                        }
                    }
                }
            }


            if ( $server_offline != $settings->server_offline ) {
                //$settings->reset();
                $settings->server_offline = $server_offline ? 'true' : '';
            }

            if ( !empty( $log_type ) && !empty( $log_message ) ) {
                switch ( $log_type ) {
                    case 'error':
                        if ( is_array( $response_info ) ) {
                            $response_info['error'] = $log_message;
                        }

                        $logger->log( $log_type, 'server', "$log_message: $url", array( 'query' => $params ) );
                        break;
                    default:
                        $logger->log( 'info', 'server', "$log_message: $url", array( 'query' => $params ) );
                        break;
                }
            }

            return $response_info;
        }

        public function get_server_api_url()
        {
            $settings=SbrSettings::instance();
            if ( $settings->https == '' || strcasecmp( $settings->https, 'true' ) == 0 ) {
                return self::SERVER_API_URL;
            } else {
                return self::SERVER_API_URL_PLAIN;
            }
        }

        public static function is_success($response_info) {
          if (is_wp_error($response_info))
            return false;
          if ( !empty( $response_info['response']['code'] ) ) {
                $response_code = $response_info['response']['code'];
                if ($response_code==200)
                  return true;
          }
          return false;
        }
    }
}
